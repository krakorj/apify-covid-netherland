// This is the main Node.js source code file of your actor.
const got = require('got');

// Include Apify SDK. For more information, see https://sdk.apify.com/
const Apify = require('apify');

Apify.main(async () => {
    // Get input of the actor (here only for demonstration purposes).
    const input = await Apify.getInput();
    console.log('Input:');
    console.dir(input);

    // Create and initialize an instance of the RequestList class that contains
    // a list of URLs to crawl. Here we use just a few hard-coded URLs.
    const requestList = new Apify.RequestList({
        sources: [
            {url: 'https://www.stichting-nice.nl/covid-19/public/global'}
        ],
    });
    await requestList.initialize();
    const kvStore = await Apify.openKeyValueStore('COVID-19-NETHERLAND');
    const dataset = await Apify.openDataset('COVID-19-NETHERLAND-HISTORY');

    // API at https://www.stichting-nice.nl/js/covid-19.js
    let body;
    try {
        const response = await got('https://www.stichting-nice.nl/covid-19/public/global', { json: true });
        body = response.body;
        console.log(body);
    } catch(error) {
        console.log(error.response.body);
    };

    //Data from global API
    let data = {};
    data.sourceUrl = 'https://www.stichting-nice.nl/covid-19/public/global';
    data.lastUpdatedAtApify = new Date();
    data.lastUpdatedAtSource = new Date();
    data.readMe = "https://apify.com/krakorj/covid-netherland";
    
    data.infectedTotal = body.count;
    data.infectedInHealthCare = body.countDistinctHospno;
    data.deathsTotal = body.countDied;
    data.deathsUpdate = 0;

    // Infected Update
    try {
        const response = await got('https://www.stichting-nice.nl/covid-19/public/new-intake/', { json: true });
        body = response.body;
        console.log(body.slice(-1)[0]);
        data.infectedUpdate = body.slice(-1)[0].newIntake;
    } catch(error) {
        console.log(error.response.body);
    };

    // Deaths Update
    try {
        const response = await got('https://www.stichting-nice.nl/covid-19/public/died-cumulative/', { json: true });
        body = response.body;
        console.log(body.slice(-1)[0]);
        data.deathsUpdate = body.slice(-1)[0].diedCumulative - body.slice(-2)[0].diedCumulative;
    } catch(error) {
        console.log(error.response.body);
    };

    // Print data
    console.log(data);

    // Store the results to the default dataset. In local configuration,
    // the data will be stored as JSON files in ./apify_storage/datasets/default
    await Apify.pushData(data);

    // OUTPUT update
    console.log('Setting OUTPUT...')
    await Apify.setValue('OUTPUT', data);

    // Key-value store / data set update
    console.log('Setting LATEST...')
    let latest = await kvStore.getValue('LATEST');
    if (!latest) {
        await kvStore.setValue('LATEST', data);
        latest = data;
        await dataset.pushData(data);
    }
    else {
        var actual = { ... data };
        var stored = { ... latest };
        delete actual.lastUpdatedAtApify;
        delete actual.lastUpdatedAtSource;
        delete stored.lastUpdatedAtApify;
        delete stored.lastUpdatedAtSource;
        if (actual !== stored) {
            await dataset.pushData(data);
        }
    }
    await kvStore.setValue('LATEST', data);

    console.log('Finished.'); 
});

    
