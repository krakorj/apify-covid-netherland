# Coronavirus stats in the Netherland

[Apify actor](https://www.apify.com) that gets Netherland stats from API endpoints at https://www.stichting-nice.nl  (referenced from https://www.rivm.nl/node/152921). Used endpoints:

* /covid-19/public/global
* /covid-19/public/new-intake
* /covid-19/public/died-cumulative

Latest data are available at this URL: https://api.apify.com/v2/key-value-stores/JZDyypYlYJtAxCwoh/records/LATEST?disableRedirect=1

You can find a dataset with unique history items here https://api.apify.com/v2/datasets/ZdYhQAMdc6OAGY1G0/items?format=json&clean=1

URL is being actualized every 5 minutes.

For more info see 

* [COVID19](https://www.covid19cz.cz/)
* [How-to-add-a-new-Covid-19-Actor](https://gitlab.com/apify-public/wiki/-/wikis/Public-actors/How-to-add-a-new-Covid-19-Actor)

